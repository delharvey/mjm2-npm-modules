{
  "name": "gulp-load-plugins",
  "version": "0.7.0",
  "description": "Automatically load any gulp plugins in your package.json",
  "scripts": {
    "test": "mocha test.js"
  },
  "repository": {
    "type": "git",
    "url": "jackfranklin/gulp-load-plugins"
  },
  "keywords": [
    "gulpfriendly",
    "gulp",
    "require",
    "load",
    "plugins"
  ],
  "author": {
    "name": "Jack Franklin"
  },
  "contributors": [
    {
      "name": "Pascal Hartig"
    },
    {
      "name": "Ben Briggs"
    },
    {
      "name": "kombucha"
    },
    {
      "name": "Nicolas Froidure"
    },
    {
      "name": "Sindre Sorhus"
    },
    {
      "name": "Julien Fontanet",
      "email": "julien.fontanet@vates.fr"
    }
  ],
  "license": "MIT",
  "dependencies": {
    "findup-sync": "^0.1.2",
    "multimatch": "1.0.0"
  },
  "devDependencies": {
    "jshint": "^2.5.1",
    "mocha": "^1.18.2",
    "proxyquire": "^1.0.1",
    "sinon": "^1.9.1"
  },
  "readme": "# gulp-load-plugins\n\n[![npm](https://nodei.co/npm/gulp-load-plugins.png?downloads=true)](https://nodei.co/npm/gulp-load-plugins/)\n\n> Loads in any gulp plugins and attaches them to the global scope, or an object of your choice.\n\n[![Build Status](https://travis-ci.org/jackfranklin/gulp-load-plugins.svg)](https://travis-ci.org/jackfranklin/gulp-load-plugins)\n\n\n## Install\n\n```sh\n$ npm install --save-dev gulp-load-plugins\n```\n\n\n## Usage\n\nGiven a `package.json` file that has some dependencies within:\n\n```json\n{\n    \"dependencies\": {\n        \"gulp-jshint\": \"*\",\n        \"gulp-concat\": \"*\"\n    }\n}\n```\n\nAdding this into your `Gulpfile.js`:\n\n```js\nvar gulp = require('gulp');\nvar gulpLoadPlugins = require('gulp-load-plugins');\nvar plugins = gulpLoadPlugins();\n```\n\nOr, even shorter:\n\n```js\nvar plugins = require('gulp-load-plugins')();\n```\n\nWill result in the following happening (roughly, plugins are lazy loaded but in practice you won't notice any difference):\n\n```js\nplugins.jshint = require('gulp-jshint');\nplugins.concat = require('gulp-concat');\n```\n\nYou can then use the plugins just like you would if you'd manually required them, but referring to them as `plugins.name()`, rather than just `name()`.\n\nThis frees you up from having to manually require each gulp plugin.\n\n## Options\n\nYou can pass in an argument, an object of options (the shown options are the defaults):\n\n```js\ngulpLoadPlugins({\n    pattern: 'gulp-*', // the glob to search for\n    config: 'package.json', // where to find the plugins, by default  searched up from process.cwd() \n    scope: ['dependencies', 'devDependencies', 'peerDependencies'], // which keys in the config to look within\n    replaceString: 'gulp-', // what to remove from the name of the module when adding it to the context\n    camelize: true, // if true, transforms hyphenated plugins names to camel case\n    lazy: true, // whether the plugins should be lazy loaded on demand\n});\n```\n\n\n## Lazy Loading\n\nIn 0.4.0 and prior, lazy loading used to only work with plugins that return a function. In newer versions though, lazy loading should work for any plugin. If you have a problem related to this please try disabling lazy loading and see if that fixes it. Feel free to open an issue on this repo too.\n\n\n## Credit\n\nCredit largely goes to @sindresorhus for his [load-grunt-plugins](https://github.com/sindresorhus/load-grunt-tasks) plugin. This plugin is almost identical, just tweaked slightly to work with Gulp and to expose the required plugins.\n\n\n## Changelog\n\n##### 0.7.0\n- support loading plugins with a dot in the name, such as `gulp.spritesmith` - thanks to @MRuy\n- upgrade multimatch to 1.0.0\n\n\n##### 0.6.0\n- Fix issues around plugin picking wrong package.json file - thanks @iliakan (see [issue](https://github.com/jackfranklin/gulp-load-plugins/issues/35)).\n\n##### 0.5.3\n- Show a nicer error if the plugin is unable to load any configuration and hence can't find any dependencies to load\n\n##### 0.5.2\n- Swap out globule for multimatch, thanks @sindresorhus.\n\n##### 0.5.1\n- Updated some internal dependencies which should see some small improvements - thanks @shinnn for this contribution.\n\n##### 0.5.0\n- improved lazy loading so it should work with plugins that don't just return a function. Thanks to @nfroidure for help with this.\n\n##### 0.4.0\n- plugins are lazy loaded for performance benefit. Thanks @julien-f for this.\n\n##### 0.3.0\n- turn the `camelize` option on by default\n\n##### 0.2.0\n- added `camelize` option, thanks @kombucha.\n- renamed to `gulp-load-plugins`.\n\n##### 0.1.1\n- add link to this repository into `package.json` (thanks @ben-eb).\n\n##### 0.1.0\n- move to `gulpLoadplugins` returning an object with the tasks define.\n\n##### 0.0.5\n- added `replaceString` option to configure exactly what gets replace when the plugin adds the module to the context\n\n##### 0.0.4\n- fixed keyword typo so plugin appears in search for gulp plugins\n\n##### 0.0.3\n- removed accidental console.log I'd left in\n\n##### 0.0.2\n- fixed accidentally missing a dependency out of package.json\n\n##### 0.0.1\n- initial release\n",
  "readmeFilename": "README.md",
  "_id": "gulp-load-plugins@0.7.0",
  "_from": "gulp-load-plugins@~0.7.0"
}
