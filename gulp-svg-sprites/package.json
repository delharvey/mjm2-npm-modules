{
  "name": "gulp-svg-sprites",
  "description": "Create SVG sprites with PNG fallbacks",
  "version": "1.0.3",
  "homepage": "",
  "repository": {
    "type": "git",
    "url": "git://github.com/shakyshane/gulp-svg-sprites.git"
  },
  "bugs": {
    "url": "https://github.com/shakyshane/gulp-svg-sprites/issues"
  },
  "licenses": [
    {
      "type": "MIT",
      "url": "/blob/master/LICENSE-MIT"
    }
  ],
  "scripts": {
    "test": "gulp lint && mocha --recursive test/specs"
  },
  "main": "index.js",
  "engines": {
    "node": ">= 0.10.0"
  },
  "dependencies": {
    "lodash": "^2.4.1",
    "svgo": "^0.4.4",
    "through2": "^0.4.1",
    "vinyl": "^0.2.3",
    "gulp-util": "^3.0.0",
    "svg-sprite-data": "0.0.5",
    "dust": "^0.3.0",
    "dustjs-linkedin": "^2.4.0",
    "q": "^1.0.1"
  },
  "keywords": [
    "svg",
    "sprites",
    "gulpplugin"
  ],
  "devDependencies": {
    "vinyl-fs": "^0.1.4",
    "gulp": "^3.8.6",
    "mocha": "~1.18.0",
    "gulp-jshint": "~1.4.0",
    "chai": "~1.9.0",
    "sinon": "~1.8.2",
    "gulp-contribs": "0.0.2",
    "gulp-clean": "^0.3.1",
    "gulp-svg2png": "^0.3.0",
    "gulp-filter": "^0.5.0",
    "gulp-yuidoc": "^0.1.2",
    "marked": "^0.3.2"
  },
  "readme": "# gulp-svg-sprites\n\nGulp plugin for working with SVGs\n\n**Notice** See [README.old.md](https://github.com/shakyShane/gulp-svg-sprites/blob/master/README.old.md) for options/example pre `1.0.0`\n\n#Version 1.0.0\n\n## Install\nInstall it locally to your project.\n\n```js\n$ npm install gulp-svg-sprites\n```\n\n##Usage\nWith no configuration, `gulp-svg-sprites` will create the following files:\n\n1. `svg/sprite.svg` - Sprite Sheet containing all of your SVGs.\n2. `sprite.html`    - A preview page with instructions & snippets.\n2. `css/sprite.css` - A CSS file with the code needed to use the sprite.\n\n```js\nvar svgSprite = require(\"gulp-svg-sprites\");\n\ngulp.task('sprites', function () {\n    return gulp.src('assets/svg/*.svg')\n        .pipe(svgSprite())\n        .pipe(gulp.dest(\"assets\"));\n});\n```\n\nThen, if you had a `facebook.svg` file, you'd be able to use the following markup in your webpage:\n\n```html\n<i class=\"icon facebook\"></i>\n```\n\n##PNG fallback\nYou can easily support old browsers by piping the new SVG sprite through to another gulp task. There will be a\n`no-svg` class generated automatically in the CSS, so you'll just need to use something like Modernizr \nto set the `no-svg` class on the `<body>` tag of your website.\n\n```js\nvar svgSprite = require(\"gulp-svg-sprites\");\nvar filter    = require('gulp-filter');\nvar svg2png   = require('gulp-svg2png');\n\ngulp.task('sprites', function () {\n    return gulp.src('assets/svg/*.svg')\n        .pipe(svgSprite())\n        .pipe(gulp.dest(\"assets\")) // Write the sprite-sheet + CSS + Preview\n        .pipe(filter(\"**/*.svg\"))  // Filter out everything except the SVG file\n        .pipe(svg2png())           // Create a PNG\n        .pipe(gulp.dest(\"assets\"));\n});\n```\n\n##Symbols mode\nPass `mode: \"symbols\"` to output SVG data as this [CSS TRICKS article](http://css-tricks.com/svg-symbol-good-choice-icons/) outlines.\nYou'll get an SVG file & a preview file showing how to use it.\n\n```js\ngulp.task('sprites', function () {\n    return gulp.src('assets/svg/*.svg')\n        .pipe(svgSprite({mode: \"symbols\"}))\n        .pipe(gulp.dest(\"assets\"));\n});\n```\n\n##Defs mode\nPass `mode: \"defs\"` to output SVG data as this [CSS TRICKS article](http://css-tricks.com/svg-sprites-use-better-icon-fonts/) outlines.\nYou'll get an SVG file & a preview file showing how to use it.\n\n```js\ngulp.task('sprites', function () {\n    return gulp.src('assets/svg/*.svg')\n        .pipe(svgSprite({mode: \"defs\"}))\n        .pipe(gulp.dest(\"assets\"));\n});\n```\n\n###Custom Selectors\nBy default, the filename will be used as the selector in the CSS, but this is how you'd override it (the `%f` will be replaced with the filename)\n \n```js\ngulp.task('sprites', function () {\n    return gulp.src('assets/svg/*.svg')\n        .pipe(svgSprite({\n            selector: \"icon-%f\"\n        }))\n        .pipe(gulp.dest(\"assets\"));\n});\n```\n\n###Custom IDs\nWith the `symbols` or `defs` mode, it's probably the ID you'll want to override. No problem.\n \n```js\ngulp.task('sprites', function () {\n    return gulp.src('assets/svg/*.svg')\n        .pipe(svgSprite({\n            svgId: \"svg-%f\"\n        }))\n        .pipe(gulp.dest(\"assets\"));\n});\n```\n\n###Custom filenames\nChange the generated filenames with ease. For example, if you want to create a `scss` partial instead, you could just do:\n \n```js\n// Custom CSS filename\ngulp.task('sprites', function () {\n    return gulp.src('assets/svg/*.svg')\n        .pipe(svgSprite({\n            cssFile: \"scss/_sprite.scss\"\n        }))\n        .pipe(gulp.dest(\"assets\"));\n});\n        \n// Custom SVG filename\ngulp.task('sprites', function () {\n    return gulp.src('assets/svg/*.svg')\n        .pipe(svgSprite({\n            svg: {\n                sprite: \"svg.svg\"\n            }\n        }))\n        .pipe(gulp.dest(\"assets\"));\n});\n        \n// Custom Preview filename + Custom SVG filename\ngulp.task('sprites', function () {\n    return gulp.src('assets/svg/*.svg')\n        .pipe(svgSprite({\n            svg: {\n                sprite: \"svg.svg\"\n            },\n            preview: {\n                sprite: \"index.html\"\n            }\n        }))\n        .pipe(gulp.dest(\"assets\"));\n});\n```\n\n###No previews\nIf you don't want 'em. Works in all modes.\n \n```js\ngulp.task('sprites', function () {\n    return gulp.src('assets/svg/*.svg')\n        .pipe(svgSprite({\n            preview: false\n        }))\n        .pipe(gulp.dest(\"assets\"));\n});\n```\n\n##Advanced: Custom Templates\nTemplates use Dustjs - checkout [their docs](https://github.com/linkedin/dustjs/wiki/Dust-Tutorial) for usage instructions. Or take a look at the [default ones](https://github.com/shakyShane/gulp-svg-sprites/blob/master/tmpl/sprite.css) for tips.\n\nYou can get your hands on JUST the SVG Data & provide your own templates. For example, if you want to provide\nyour own template for the CSS output, you could do this:\n\n```js\nvar config = {\n    templates: {\n        css: require(\"fs\").readFileSync(\"./path/to/your/template.css\", \"utf-8\")\n    }\n};\n\ngulp.task('sprites', function () {\n    return gulp.src('assets/svg/*.svg')\n        .pipe(svgSprite(config))\n        .pipe(gulp.dest(\"assets\"));\n});\n```\n\nYou can override all the [templates used](https://github.com/shakyShane/gulp-svg-sprites/blob/master/index.js#L57-L64) in the same way.\n\n##Advanced: Data Transforms\nIf you want to do some custom stuff with your templates, you might need to transform the SVG data before it gets to your template. There\nare two functions you can provide to do this & they'll override the internal ones. Override `transformData` and you'll have direct access\nto the data returned from [svg-sprite-data](https://github.com/shakyShane/svg-sprite-data). This will skip the few transformation that \nthis library applies - so use with caution. (if you want to modify the data aswell after our internal modifications, use `afterTransform` instead).\n\n```js\nvar config = {\n    transformData: function (data, config) {\n        return data; // modify the data and return it\n    },\n    templates: {\n        css: require(\"fs\").readFileSync(\"./path/to/your/template.css\", \"utf-8\")\n    }\n};\n\ngulp.task('sprites', function () {\n    return gulp.src('assets/svg/*.svg')\n        .pipe(svgSprite(config))\n        .pipe(gulp.dest(\"assets\"));\n});\n\n```\n\nYou can override all the [templates used here](https://github.com/shakyShane/gulp-svg-sprites/blob/master/index.js#L172-L179) in the same way.\n\n## Options\n<table>\n<thead>\n<tr>\n<th>Name</th>\n<th>Type</th>\n<th>Default</th>\n<th>Description</th>\n</tr>\n</thead>\n<tbody>\n\n<tr>\n    <td><b>mode</b></td>\n    <td>String</td>\n    <td><code>sprite</code></td>\n    <td><p>Define which mode to run in. Can be either &quot;sprite&quot;, &quot;defs&quot; or &quot;symbols&quot;</p>\n</td>\n</tr>\n\n\n<tr>\n    <td><b>common</b></td>\n    <td>String</td>\n    <td><code>icon</code></td>\n    <td><p>By default, the class <code>icon</code> will be used as the common class.\n but you can also choose your own</p>\n</td>\n</tr>\n\n\n<tr>\n    <td><b>selector</b></td>\n    <td>String</td>\n    <td><code>%f</code></td>\n    <td><p>Easily add prefixes/suffixes to the generated CSS classnames. The <code>%f</code> will\n be replaced by the filename</p>\n</td>\n</tr>\n\n\n<tr>\n    <td><b>layout</b></td>\n    <td>String</td>\n    <td><code>vertical</code></td>\n    <td><p>Define the layout of the items in the sprite. Can be either\n &quot;vertical&quot;, &quot;horizontal&quot; or &quot;diagonal&quot;</p>\n</td>\n</tr>\n\n\n<tr>\n    <td><b>svgId</b></td>\n    <td>String</td>\n    <td><code>%f</code></td>\n    <td><p>In <code>symbols</code> or <code>defs</code> mode, you&#39;ll probably want to override the ID on each element.\n The filename will be used as a default, but can be overridden.</p>\n</td>\n</tr>\n\n\n<tr>\n    <td><b>cssFile</b></td>\n    <td>String</td>\n    <td><code>css/sprite.css</code></td>\n    <td><p>Define the path &amp; filename of the CSS file. Using this, you could easily create a SASS\n partial for example</p>\n</td>\n</tr>\n\n\n<tr>\n    <td><b>svgPath</b></td>\n    <td>String</td>\n    <td><code>../%f</code></td>\n    <td><p>Define the path to the SVG file that be written to the CSS file. Note: this does NOT alter\n the actual write-path of the SVG file. See the <code>svg</code> option for that.</p>\n</td>\n</tr>\n\n\n<tr>\n    <td><b>pngPath</b></td>\n    <td>String</td>\n    <td><code>../%f</code></td>\n    <td><p>If you&#39;re creating a PNG fallback, define the path to it that be written to the CSS file.</p>\n</td>\n</tr>\n\n\n<tr>\n    <td><b>preview</b></td>\n    <td>Object</td>\n    <td></td>\n    <td><p>Paths to preview files.</p>\n</td>\n</tr>\n\n<tr>\n    <td> preview.sprite</td>\n    <td>String</td>\n    <td><code>sprite.html</code></td>\n    <td></td>\n</tr>\n\n<tr>\n    <td> preview.defs</td>\n    <td>String</td>\n    <td><code>defs.html</code></td>\n    <td></td>\n</tr>\n\n<tr>\n    <td> preview.symbols</td>\n    <td>String</td>\n    <td><code>symbols.html</code></td>\n    <td></td>\n</tr>\n\n\n<tr>\n    <td><b>svg</b></td>\n    <td>Object</td>\n    <td></td>\n    <td><p>Paths to SVG files.</p>\n</td>\n</tr>\n\n<tr>\n    <td> svg.sprite</td>\n    <td>String</td>\n    <td><code>svg/sprite.svg</code></td>\n    <td></td>\n</tr>\n\n<tr>\n    <td> svg.defs</td>\n    <td>String</td>\n    <td><code>svg/defs.svg</code></td>\n    <td></td>\n</tr>\n\n<tr>\n    <td> svg.symbols</td>\n    <td>String</td>\n    <td><code>svg/symbols.svg</code></td>\n    <td></td>\n</tr>\n\n\n<tr>\n    <td><b>padding</b></td>\n    <td>Number</td>\n    <td><code>0</code></td>\n    <td><p>Add padding to sprite items</p>\n</td>\n</tr>\n\n\n<tr>\n    <td><b>transformData</b></td>\n    <td>Function</td>\n    <td><code>transformData</code></td>\n    <td><p>Override the default data transforms</p>\n</td>\n</tr>\n\n\n<tr>\n    <td><b>afterTransform</b></td>\n    <td>Function</td>\n    <td><code>afterTransform</code></td>\n    <td><p>Apply additional data transforms AFTER the defaults</p>\n</td>\n</tr>\n\n\n</tbody>\n</table>\n## License\nCopyright (c) 2014 Shane Osbourne\nLicensed under the MIT license.\n",
  "readmeFilename": "README.md",
  "_id": "gulp-svg-sprites@1.0.3",
  "_from": "gulp-svg-sprites@^1.0.3"
}
